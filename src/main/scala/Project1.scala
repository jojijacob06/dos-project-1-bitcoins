import java.security.MessageDigest

import akka.actor._
import akka.routing.{RoundRobinPool, RoundRobinRouter}

import scala.collection.immutable.StringOps
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.util.Random


sealed trait ProjectOneMessages
case class MasterInitAsServer(numberOfZeros: Int) extends ProjectOneMessages
case object TerminateServer extends ProjectOneMessages
case object ForceTerminateServer extends ProjectOneMessages
case class WorkerInit(numberOfZeros: Int) extends ProjectOneMessages
case object RandomStringsRequestWtoM extends ProjectOneMessages
case class RandomStringsResponseMtoW(randomStrings: List[String]) extends ProjectOneMessages
case class BitcoinsResponseWtoM(bitcoins: List[(Int, (String, String))]) extends ProjectOneMessages
case object IncreaseRandomStringLength extends ProjectOneMessages

case class MasterInitAsNetworkNode(ipAddress: String) extends ProjectOneMessages
case object JoinRequestNtoM extends ProjectOneMessages
case class JoinResponseMtoN(numberOfZeros: Int) extends ProjectOneMessages
case object RandomStringsRequestNtoM extends ProjectOneMessages
case class RandomStringsResponseMtoN(randomStrings: List[String]) extends ProjectOneMessages
case class BitcoinsResponseNtoM(bitcoins: List[(Int, (String, String))]) extends ProjectOneMessages

class Master extends Actor with ActorLogging {
  import context._
  var zeros: Int = _
  val numberOfWorkersPerNode: Int = 4
  var randomStringLength: Int = 24
  val numberOfRandomStringsPerBlock: Int = 400
  val timeoutDuration = Duration(60000, "millis")
  val terminationRetryDuration = Duration(500, "millis")
  val forceTerminationDuration = Duration(5000, "millis")
  var terminateCancellable: Cancellable = _
  var forceTerminateCancellable: Cancellable = _
  val increaseRandomStringLengthDuration = Duration(10000, "millis")
  var increaseRandomStringLengthCancellable: Cancellable = _
  var outstandingRequests: Int = 0
  var terminationInProgress: Boolean = false
  var localBitcoinsCount: Int = 0
  var remoteBitcoinsCount: Int = 0
  var mostLeadingZeroBitcoin: (Int, (String, String)) = (0, ("", ""))
  var numberOfRequestsToWorkers: Int = 0
  var numberOfRemoteRequests: Int = 0

  def createNewRandomStringWithAlias(numberOfStrings: Int, length: Int): List[String] = {
    val randomStrings = new ListBuffer[String]()
    for (i <- 1 to numberOfStrings)
      randomStrings += "j.jacob;" + Random.alphanumeric.take(length).mkString
    randomStrings.toList
  }

  def receive = {
    case MasterInitAsServer(numberOfZeros) =>
      terminateCancellable = context.system.scheduler.scheduleOnce(timeoutDuration, self, TerminateServer)
      forceTerminateCancellable = context.system.scheduler.scheduleOnce(timeoutDuration + forceTerminationDuration, self, ForceTerminateServer)
      increaseRandomStringLengthCancellable = context.system.scheduler.schedule(increaseRandomStringLengthDuration, increaseRandomStringLengthDuration, self, IncreaseRandomStringLength)
      zeros = numberOfZeros
      (0 until numberOfWorkersPerNode).foreach { 
        case i => 
              context.system.actorOf(Props(new Worker), s"Worker$i") ! WorkerInit(numberOfZeros)
      }

    case IncreaseRandomStringLength =>
      randomStringLength += 1

    case RandomStringsRequestWtoM =>
      if (!terminationInProgress) {
        sender ! RandomStringsResponseMtoW(createNewRandomStringWithAlias(numberOfRandomStringsPerBlock, randomStringLength))
        outstandingRequests += 1
        numberOfRequestsToWorkers += 1
      }

    case BitcoinsResponseWtoM(bitcoins) =>
      outstandingRequests -= 1
      localBitcoinsCount += bitcoins.length
      for (bitcoin <- bitcoins) {
        println (bitcoin._2._1 + "    " + bitcoin._2._2)
        if (bitcoin._1 > mostLeadingZeroBitcoin._1) {
          mostLeadingZeroBitcoin = bitcoin
        }
      }

    case JoinRequestNtoM =>
      sender ! JoinResponseMtoN(zeros)

    case RandomStringsRequestNtoM =>
      if (!terminationInProgress) {
        sender ! RandomStringsResponseMtoN(createNewRandomStringWithAlias(numberOfRandomStringsPerBlock*4, randomStringLength))
        numberOfRemoteRequests += 1
      }

    case BitcoinsResponseNtoM(bitcoins) =>
      remoteBitcoinsCount += bitcoins.length
      for (bitcoin <- bitcoins) {
        println (bitcoin._2._1 + "\t" + bitcoin._2._2)
        if (bitcoin._1 > mostLeadingZeroBitcoin._1) {
          mostLeadingZeroBitcoin = bitcoin
        }
      }

    case TerminateServer =>
      terminationInProgress = true
      if (0 == outstandingRequests) {
        println(s"local bitcoins found $localBitcoinsCount")
        println(s"remote bitcoins found $remoteBitcoinsCount")
        println("total " + (localBitcoinsCount+remoteBitcoinsCount))
        println("most number of leading zeros " + mostLeadingZeroBitcoin._1)
        println(s"number of requests to workers $numberOfRequestsToWorkers")
        println(s"number of remote requests $numberOfRemoteRequests")
        println(mostLeadingZeroBitcoin._2._1 + "\t" + mostLeadingZeroBitcoin._2._2)
        increaseRandomStringLengthCancellable.cancel()
        forceTerminateCancellable.cancel()
        context.system.shutdown()
      }
      else terminateCancellable = context.system.scheduler.scheduleOnce(terminationRetryDuration, self, TerminateServer)

    case ForceTerminateServer =>
      println(s"local bitcoins found $localBitcoinsCount")
      println(s"remote bitcoins found $remoteBitcoinsCount")
      println("total " + (localBitcoinsCount+remoteBitcoinsCount))
      println("most number of leading zeros " + mostLeadingZeroBitcoin._1)
      println(mostLeadingZeroBitcoin._2._1 + "\t" + mostLeadingZeroBitcoin._2._2)
      increaseRandomStringLengthCancellable.cancel()
      terminateCancellable.cancel()
      context.system.shutdown()
  }
}

class MasterOnANetworkNode extends Actor with ActorLogging {
  import context._
  val numberOfWorkersPerNode: Int = 4
  var localRequestsReceived: Int = 0
  var masterServerSelection: ActorSelection = _
  var workerRefs: ListBuffer[ActorRef] = new ListBuffer[ActorRef]()
  var outstandingRequests: Int = 0
  var bigListOfAllDigests: ListBuffer[(Int, (String, String))] = new ListBuffer[(Int, (String, String))]()
  var localBitcoinsFound: Int = 0
  val serverResponseTimeoutDuration = Duration(3000, "millis")
  var serverResponseWaitCancellable: Cancellable = _

  def getActorPath(ipAddress: String): String = "akka.tcp://Project1@" + ipAddress + ":2552/user/Master"

  def receive = {
    case MasterInitAsNetworkNode(ipAddress) =>
      masterServerSelection = context.actorSelection(getActorPath(ipAddress))
      masterServerSelection ! JoinRequestNtoM
      serverResponseWaitCancellable = context.system.scheduler.scheduleOnce(serverResponseTimeoutDuration, self, TerminateServer)

    case JoinResponseMtoN(numberOfZeros) =>
      serverResponseWaitCancellable.cancel()
      for(i <- 0 until numberOfWorkersPerNode) {
        val workerRef = context.system.actorOf(Props(new Worker), s"Worker$i")
        workerRefs += workerRef
        workerRef ! WorkerInit(numberOfZeros)
      }

    case RandomStringsRequestWtoM =>
      localRequestsReceived += 1
      if (localRequestsReceived == numberOfWorkersPerNode) {
        masterServerSelection ! RandomStringsRequestNtoM
        serverResponseWaitCancellable = context.system.scheduler.scheduleOnce(serverResponseTimeoutDuration, self, TerminateServer)
        localRequestsReceived = 0
      }
      
    case RandomStringsResponseMtoN(randomStrings) =>
      serverResponseWaitCancellable.cancel()
      val smallGroupsOfRandomStrings = randomStrings.grouped(randomStrings.length/4).toList
      for (i <- smallGroupsOfRandomStrings.indices) {
        workerRefs(i) ! RandomStringsResponseMtoW(smallGroupsOfRandomStrings(i))
        outstandingRequests += 1
      }

    case BitcoinsResponseWtoM(bitcoins) =>
      outstandingRequests -= 1
      localBitcoinsFound += bitcoins.length
      bitcoins.foreach {
        bigListOfAllDigests += _
      }
      if (outstandingRequests == 0) {
        masterServerSelection ! BitcoinsResponseNtoM(bigListOfAllDigests.toList)
        bigListOfAllDigests.clear()
      }

    case TerminateServer =>
      println(s"local bitcoins found $localBitcoinsFound")
      context.system.shutdown()
  }
}

class Worker extends Actor with ActorLogging {
  var zeros: Int = _

  def Sha256(s: String): String = {
    val m = MessageDigest.getInstance("SHA-256").digest(s.getBytes("UTF-8"))
    m.map("%02x".format(_)).mkString
  }

  def NotAZero(c: Char): Boolean = c != '0'

  def getNumberOfLeadingZeros(digest: String): Int = {
    val s = new StringOps(digest)
    s.indexWhere(NotAZero)
  }

  def receive = {
    case WorkerInit(numberOfZeros) =>
      zeros = numberOfZeros
      sender ! RandomStringsRequestWtoM

    case RandomStringsResponseMtoW(randomStrings) =>
      val bitcoins = new ListBuffer[(Int, (String, String))]()
      for (randomString <- randomStrings) {
        val digest = Sha256(randomString)
        val zerosInDigest = getNumberOfLeadingZeros(digest)
        if (zerosInDigest >= zeros) {
          val bitcoin = (zerosInDigest, (randomString, digest))
          bitcoins += bitcoin
        }
      }
      sender ! BitcoinsResponseWtoM(bitcoins.toList)
      sender ! RandomStringsRequestWtoM
  }
}

object Project1 {
  def main (args: Array[String]) {
    if (args(0).mkString.contains(".")) ActorSystem("Project1-N").actorOf(Props(new MasterOnANetworkNode), "MasterN") ! MasterInitAsNetworkNode(args(0))
    else ActorSystem("Project1").actorOf(Props(new Master), "Master") ! MasterInitAsServer(args(0).toInt)
  }
}
