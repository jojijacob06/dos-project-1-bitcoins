# README #

## DOS(COP5615) Project 1 ##

* Find bitcoins with required number of leading zeros (say '4')
* Uses Scala and the actor model to distribute work in a master-worker model
* Distributed implementation that uses network nodes to find bitcoins

## Project Group ##

* Joji Jacob (UFID: 32318141)
* Riju John Xavier

## Size of the work unit ##

## Program output ##

## CPU time to REAL time ##

## Coins with the most zeros ##

## Largest number of working machines involved in mining ##

## How do I get set up? ##

### On node acting as server, enter the following commands in terminal at the project root ###
* $ sbt
* $ compile
* $ run 4

### On node acting as worker, enter the following ###
* $ sbt
* $ compile
* $ run <server-ip-adress>

## Who do I talk to? ##

* jojijacob06@gmail.com
* Riju John Xavier